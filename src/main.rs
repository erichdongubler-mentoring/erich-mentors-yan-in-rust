extern crate toml; // 0.5.3

use std::collections::HashMap;
use toml::Value;

pub fn get_sections(schema: &Value) -> HashMap<String, bool> {
    schema["sections"].as_table().unwrap().into_iter().map(|(key, value)| {
        (key.clone(), value["required"].as_bool().unwrap())
    }).collect()
}

fn main() {
    let schema = "[sections]
RUNSPEC  = {required = true}
GRID     = {required = true}";

    let schema: Value = toml::from_str(&schema).unwrap();

    let sections = get_sections(&schema);
    println!("{:?}", sections);
}
